def linearSearch(e, a):
    for index, value in enumerate(a, 1):
        if e == value:
            return index
    return -1


array = [4, 5, 7, 2, 9, 6]
element = 9
index_value = linearSearch(element, array)
print(f"The element is found at the position {index_value} in the array.")
