def binarySearch(e, a):
    left_pointer = 0
    right_pointer = len(a) - 1

    if (e > a[right_pointer]) or (e < a[left_pointer]):
        return -1

    while (left_pointer < right_pointer):
        mid = left_pointer + (right_pointer - left_pointer) // 2
        if e > a[mid]:
            left_pointer = mid + 1
        elif e < a[mid]:
            right_pointer = mid - 1
        elif e == a[mid]:
            return (mid)

    return (-1)


array = [4, 5, 7, 12, 19, 26, 30, 31, 34, 36, 37, 39, 53, 64, 87, 89, 90]
element = 75
index_value = binarySearch(element, array)
print(f"The element is found at the position {index_value} in the array.")
